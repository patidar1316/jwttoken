package com.web.PrecticeProject.service;

import com.web.PrecticeProject.model.User;
import com.web.PrecticeProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    private UserRepository repo;
    private List<User> store=new ArrayList<User>();

    public User saveUser(@RequestBody User user) {
    return repo.save(user);
    }

    public List<User> fetchDetails() {
        return repo.findAll();
    }
    /*public UserService(){
   store.add(new User(UUID.randomUUID().toString(),"Nikita patidar","nikita@123.gmail.com"));
        store.add(new User(UUID.randomUUID().toString(),"Rohini","Rohini@123.gmail.com"));
        store.add(new User(UUID.randomUUID().toString(),"Syama","syama@123.gmail.com"));
        store.add(new User(UUID.randomUUID().toString(),"sofia","sofia@123.gmail.com"));

    }*/
    /*public List<User> getUsers(){
        return this.store;
    }*/
}
