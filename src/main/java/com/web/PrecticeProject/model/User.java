package com.web.PrecticeProject.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="User_table")
public class User {
    @Id
    @Column
    private Long  userId;
    @Column
    private String  name;
    @Column
    private String  email;
}
