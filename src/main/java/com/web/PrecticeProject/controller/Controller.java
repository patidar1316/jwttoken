package com.web.PrecticeProject.controller;

import com.web.PrecticeProject.model.User;
import com.web.PrecticeProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class Controller {


    @Autowired
    private UserService userService;
    @PostMapping("/hello")
    public String testingMethod(){
        return "All Okay";
    }

    @PostMapping("/save")
    public User saveUser(@RequestBody User user){
          return userService.saveUser(user);
        }
        @GetMapping("/findAll")
    public List<User> fetchDetails(){
        return userService.fetchDetails();
    }
    @GetMapping("/current-user")
    public String getLoggedInUser(Principal principal){
        return principal.getName();
    }

}
